package com.dell.A;

public class DataAccess {
	//Private is accassible within a class.
	
	private int privateV;
	
	// default is accessible within a package
	int defaultV;
	//protected is accessible within a class, package,
	//outside the package but with inheritance.
	
	private static int prVar;//private variable
	static int dVar;//default variable
	protected int proVar;//protected variable
	public int puVar;//public variable
	//inner class
	private class PrivateClass{}
	class Dclass{}
	
	protected class proClass{}
	
	public class Pubclass{}
	
	private void privateMethod() {};
void defaultMethod() {};
protected int protectedMethod() {return 10;}
	public static void main(String[] args) {
		System.out.println("data,"+prVar);
		System.out.println("data,"+dVar);
	 
 }
} 	