package com.dell.A;

public class ObjectTypeConversion {
	public static void main(String[] args){
		
		//converting a primitive to Object type
		int number = 100;
		int number2= 10002;
		Integer obj = Integer.valueOf(number);
		Integer objnum= number2; //implicittype cov -> Autoboxing
		System.out.println(number + " " + obj);
		System.out.println(number2 + " " + objnum);
		
		
		//converting obj type to primitive type
		Integer obj2 = new Integer(100);
		Integer intobj2 = new Integer(500);
		int num = obj2.intValue(); // Converting object to primitive
		int num2 = intobj2; //autoUnboxing 
		System.out.println(num2 + " " +intobj2); 
		
		float f = 1234.5f;
		Float obj3 = Float.valueOf(f);
		System.out.println(f+" "+ obj3);
		
		Float obj4=new Float(1234.5f);
		float f1 = obj4.floatValue();
		System.out.println(f1+ " "+obj4);
		
	}

}
